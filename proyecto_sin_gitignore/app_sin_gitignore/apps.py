from django.apps import AppConfig


class AppSinGitignoreConfig(AppConfig):
    name = 'app_sin_gitignore'
